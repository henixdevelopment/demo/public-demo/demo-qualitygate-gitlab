metadata:
    name: Test
jobs:
    gitlab-results:
        runs-on: ssh
        generator: tm.squashtest.org/tm.generator@v1
        with:
            squashTMUrl: https://nightly-mariadb.acceptance.squashtest.org
            squashTMAutomatedServerLogin: taserver
            squashTMAutomatedServerPassword: squash123
            testPlanUuid: 4e86e1ba-1b9a-4c7a-85a4-206785dc67f7
            testPlanType: Iteration
